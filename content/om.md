---
title: "Kollektivforeningen"
type: "single"
---

Kollektivforeningen har som formål at:

1. udbrede kendskabet til kollektive boformer
1. støtte opstart af nye kollektiver
1. sikre den fortsatte eksistens af eksisterende kollektiver
1. gøre det nemmere og mere attraktivt at bo kollektivt

