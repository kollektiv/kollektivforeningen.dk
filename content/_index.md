---
title: "Kollektivforeningen"
---

Kære kollektiver i Kollektivforeningen (tidl. Foreningen for Kollektivers Fremme),

Vi har lavet en arbejdsgruppe, der vil gøre det nemmere for folk at låne penge til nye kollektiver og til at finde en fælles indbo- og ansvarsforsikring. Derfor vil vi gerne spørge dig kort om jeres kollektivs erfaringer med bank og forsikring:

1. Er jeres kollektiv sameje, lille andelsboligforening/foreningsejet, leje, andelsbolig, fondsejet, eller endda noget andet?
1. Har I lån i en bank? Hvilken?
1. Har I et lån i et realkreditinstitut? Hvilket?
1. Vil I anbefale andre at bruge jeres bank/realkreditinstitut, hvis de skulle stifte et kollektiv? Hvis du kender det, tilføj gerne navn, filiale og kontakt til jeres bankrådgiver 
1. Har I en fælles indbo- og ansvarsforsikring i jeres kollektiv?
1. Vil I anbefale jeres forsikringsselskab til andre kollektiver?  Hvis du kender det: hvilket forsikringsselskab + evt. navn, mail, telefon på kontaktperson
1. Må vi kontakte dig for at få flere oplysninger, hvis relevant?

Send gerne svarene til [kollektivforeningen@gmail.com](mailto:kollektivforeningen@gmail.com)

Når vi har samlet alle de gode kontakter hos banker og forsikringsselskaber, så sørger vi for at de bliver formidlet til nye og gamle kollektivister.
